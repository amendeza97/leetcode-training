/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

 func verifyNodes(left *TreeNode, right *TreeNode) bool {
    if (left != nil && right == nil) || (left == nil && right != nil) {
        return false
    }

    if left == nil && right == nil {
        return true
    }

    areEqualNodes := left.Val == right.Val
    if !areEqualNodes {
        return false
    }

    return verifyNodes(left.Left, right.Right) && verifyNodes(left.Right, right.Left)
}

func isSymmetric(root *TreeNode) bool {
    return verifyNodes(root.Left, root.Right)
}