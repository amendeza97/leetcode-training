/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function maxDepth(root: TreeNode | null, accumDepth: number = 1): number {
    if (!root) return 0;
    if (!root.left && !root.right) return accumDepth;
    
    let leftDepth = 0;
    if (root.left) {
        leftDepth = maxDepth(root.left, accumDepth + 1);
    }

    let rightDepth = 0;
    if (root.right) {
        rightDepth = maxDepth(root.right, accumDepth + 1);
    }

    return rightDepth > leftDepth ? rightDepth : leftDepth;
};