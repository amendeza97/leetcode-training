/**
 * @param {number[]} heights
 * @return {number}
 */
var heightChecker = function(heights) {
    const sortedHeights = [...heights].sort((a, b) => a - b);
    let counter = 0;
    heights.forEach((height, index) => {
        if (height !== sortedHeights[index]) counter++;
    })
    return counter;
};