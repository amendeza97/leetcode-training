/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function minDepth(root: TreeNode | null): number {
    if (!root) return 0;
    
    if (!root.left && !root.right) return 1;

    let leftDepth = 0;
    if (root.left) {
        leftDepth = minDepth(root.left) + 1;
    }

    let rightDepth = 0;
    if (root.right) {
        rightDepth = minDepth(root.right) + 1;
    }

    if (!root.left) {
        return rightDepth;
    }
    if (!root.right) {
        return leftDepth;
    }
    return leftDepth < rightDepth ? leftDepth : rightDepth;
};