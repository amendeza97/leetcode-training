const generateRow = (result, currentRow, numRows) => {
    if (currentRow > numRows) return;
    
    const rowIndex = currentRow - 1;
    result.push([1]);
    for (let index = 1; index < currentRow - 1; index++) {
        const leftUp = result[rowIndex - 1][index - 1];
        const rightUp = result[rowIndex - 1][index];
        result[rowIndex][index] = leftUp + rightUp;
    }
    result[rowIndex].push(1)
    generateRow(result, currentRow + 1, numRows);
}

/**
 * @param {number} numRows
 * @return {number[][]}
 */
function generate(numRows) {
    const result = [[1]];
    generateRow(result, 2, numRows)
    return result;
};