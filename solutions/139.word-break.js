/**
 * @param {string} s
 * @param {string[]} wordDict
 * @return {boolean}
 */
var wordBreak = function(s, wordDict) {
    let restWord = s;
    let [isFinished, result] = [false, false];
    while (!isFinished) {
        let newRest = '';
        for (const word of wordDict) {
            if (restWord.startsWith(word)) {
                newRest = restWord.slice(word.length);
                restWord = restWord.replace(word, '');
                result = !Boolean(restWord);
                break;
            }
        }
        isFinished = !Boolean(newRest) || result === true;
    }
    return result;
};