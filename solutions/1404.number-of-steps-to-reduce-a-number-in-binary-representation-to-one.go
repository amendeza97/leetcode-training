import (
	"regexp"
	"strings"
)

func isZeroOrOneBinary(binary string) bool {
	match, _ := regexp.MatchString("^0*(1|0)$", binary)
	return match
}

func numSteps(s string) int {
	var steps int = 0
	var binaryNumber string = s
	var foundAnswer bool = isZeroOrOneBinary(binaryNumber)
	for !foundAnswer {
		var binaryNumberLength int = len(binaryNumber)
		var lastIndex int = binaryNumberLength - 1
		var lastDigit string = string(binaryNumber[lastIndex])
		isEven := lastDigit == "0"
		if isEven {
			binaryNumber = binaryNumber[:lastIndex]
		} else {
			lastZeroIndex := strings.LastIndex(binaryNumber, "0")
			if lastZeroIndex >= 0 {
				zerosQuantity := len(binaryNumber) - lastZeroIndex - 1
				leftPart := binaryNumber[:lastZeroIndex]
				parts := []string{leftPart, strings.Repeat("1", zerosQuantity), "0"}
				binaryNumber = strings.Join(parts, "")
			} else {
				binaryNumber = "1" + strings.Repeat("0", len(binaryNumber))
			}
		}
		steps = steps + 1
		foundAnswer = isZeroOrOneBinary((binaryNumber))
	}
	return steps
}