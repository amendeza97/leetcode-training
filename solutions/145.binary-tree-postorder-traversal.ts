/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function calculatePostOrderTraversal(root: TreeNode | null, result: number[]): void {
    if (!root) return;

    if (root.left) {
        calculatePostOrderTraversal(root.left, result);
    }

    if (root.right) {
        calculatePostOrderTraversal(root.right, result);
    }

    result.push(root.val);
};

function postorderTraversal(root: TreeNode | null): number[] {
    const result = [];
    calculatePostOrderTraversal(root, result)
    return result;
};