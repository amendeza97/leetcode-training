function majorityElement(nums: number[]): number {
    const counters = {};
    let max = { num: 0, occurrencies: 0 };
    nums.forEach(num => {
        let occurrency = 1;
        if (num in counters) {
            occurrency = counters[num] + 1;
        }
        counters[num] = occurrency;
        if (occurrency > max.occurrencies) {
            max = { num, occurrencies: occurrency } 
        }
    })
    return max.num;
};