function maxAscendingSum(nums: number[]): number {
    if (!nums.length) return 0;
  
    let maxSum = 0;
    let sum = nums[0];
    let prev = nums[0];
    for (let idx = 1; idx < nums.length; idx++) {
      const num = nums[idx];
      if (num > prev) {
          sum += num;
          prev = num;
          continue;
      }
  
      if (sum > maxSum) {
          maxSum = sum;
      }
      sum = num;
      prev = num;
    }
    if (sum > maxSum) {
      maxSum = sum;
    }
    return maxSum;
  };