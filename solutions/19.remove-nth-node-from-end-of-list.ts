/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

const remove = (node: ListNode | null, n: number): {result: ListNode | null, c: number} => {
    if (!node) {
        return { result: null, c: 0 };
    }
    const { result, c } = remove(node.next, n);
    node.next = result;
    const nodePosition = c + 1;
    if (nodePosition === n) {
        return { result, c: nodePosition }
    }
    return { result: node, c: nodePosition }
};

function removeNthFromEnd(head: ListNode | null, n: number): ListNode | null {
    const { result } =  remove(head, n);
    return result;
};