import (
	"fmt"
	"strconv"
	"strings"
)

func getLucky(s string, k int) int {
	alphabet := "abcdefghijklmnopqrstuvwxyz"
	numsChars := ""
	accum, counter, index := 0, 1, 0

	for _, v := range s {
		letter := string(v)
		num := strings.Index(alphabet, letter) + 1
		numsChars = fmt.Sprintf("%s%d", numsChars, num)
	}

	for counter <= k {
		numChar := string(numsChars[index])
		value, _ := strconv.Atoi(numChar)
		accum += value

		index++
		if index == len(numsChars) {
			numsChars = strconv.Itoa(accum)
			index = 0
			if counter < k {
				accum = 0
			}
			counter++
		}
	}
	return accum
}
