function isHappy(n: number): boolean {
    let num = n;
    let sum = 0;
    let count = 0;
    while (count < 1000) {
        count++;
        sum += Math.pow(num % 10, 2)
        num = Math.trunc(num / 10);
        if (!num) {
            num = sum;

            if (sum === 1) return true;
            sum = 0;
        }
    };
    return false;
};