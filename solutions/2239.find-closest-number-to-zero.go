func abs(number int) int {
	if number < 0 {
		return number * (-1)
	}
	return number
}

func findClosestNumber(nums []int) int {
	var indexClosest = 0
	for index := 1; index < len(nums); index++ {
		var element = nums[index]

		resultElement := nums[indexClosest]
		var absElement, absResultElement = abs(element), abs(resultElement)
		if absElement < absResultElement {
			indexClosest = index
			continue
		}

		if absElement == absResultElement {
			if element > 0 {
				indexClosest = index
			}
		}
	}
	return nums[indexClosest]
}
