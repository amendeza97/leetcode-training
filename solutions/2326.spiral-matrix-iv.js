/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {number} m
 * @param {number} n
 * @param {ListNode} head
 * @return {number[][]}
 */
var spiralMatrix = function(m, n, head) {
    const result = Array(m).fill().map(() => Array(n).fill(-1));
    let node = head;
    const movs = [[0, 1], [1, 0], [0, -1], [-1, 0]];
    let [row, col, mov] = [0, 0, 0];

    while (node) {
        result[row][col] = node.val;

        const [movRow, movCol] = movs[mov];
        const [nextRow, nextCol] = [row + movRow, col + movCol];
        if (result[nextRow]?.[nextCol] !== -1) {
            mov = ((mov + 1) % movs.length);
        }
        row += movs[mov][0];
        col += movs[mov][1];

        node = node.next;
    }
    return result;
};