/**
 * Definition for singly-linked list.
 * class ListNode {
 *     val: number
 *     next: ListNode | null
 *     constructor(val?: number, next?: ListNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.next = (next===undefined ? null : next)
 *     }
 * }
 */

function swapPairs(head: ListNode | null): ListNode | null {
    if (!head) return null;

    const result = head.next;
    let node = head, nextNode = head?.next, prevNodeRef = null;
    while (nextNode) {
        let aux = nextNode.next;
        node.next = aux;
        nextNode.next = node;
        
        if (prevNodeRef) prevNodeRef.next = nextNode;

        prevNodeRef = node;
        node = aux;
        nextNode = aux?.next;
    }
    return result ?? head;
};