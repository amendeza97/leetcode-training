package main

import "fmt"

func equalFrequency(word string) bool {
	var m = make(map[string]int)

	for index := range word {
		letter := string(word[index])
		counter, exists := m[letter]
		if !exists {
			m[letter] = 1
			continue
		}
		m[letter] = counter + 1
	}
	var counter1, counter2 = 0, 0
	for _, counter := range m {
		fmt.Printf("counter %d | counter1 = %d | counter2 = %d\n", counter, counter1, counter2)
		if counter == counter1 || counter == counter2 {
			continue
		}
		if counter1 == 0 {
			counter1 = counter
			continue
		}
		if counter2 == 0 {
			counter2 = counter
			continue
		}
		if counter != counter1 && counter != counter2 {
			return false
		}
	}
	fmt.Println(m)

	return counter1-counter2 == 1 || counter1-counter2 == -1
}

func main() {
	fmt.Println(equalFrequency("aazz"))
}
