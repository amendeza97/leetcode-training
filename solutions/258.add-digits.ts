function addDigits(num: number): number {
    let result = 0;
    let restOfNumber = num;
    while (restOfNumber) {
        let lastDigit = restOfNumber % 10;
        restOfNumber = Math.trunc(restOfNumber / 10);
        result += lastDigit;
        if (!restOfNumber && result > 9) {
            restOfNumber = result;
            result = 0;
        }
    };
    return result;
};