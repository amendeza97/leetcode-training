const isPrime = num => {
    for(let i = 2, s = Math.sqrt(num); i <= s; i++) {
        if(num % i === 0) return false;
    }
    return num > 1;
}

/**
 * @param {number[][]} nums
 * @return {number}
 */
var diagonalPrime = function(nums) {
    let largestPrime = 0;
    let rowAccum = 0, length = nums.length - 1;

    const isLargestPrime = (num) => {
        if (num === 1) return false;

        const result = isPrime(num);
        return result && num > largestPrime;
    }

    for (let idx = 0; idx <= length; idx++) {
        let number1 = nums[idx][rowAccum];
        let number2 = nums[length - idx][rowAccum];

        if (isLargestPrime(number1)) largestPrime = number1;
        if (number2 !== number1 && isLargestPrime(number2)) largestPrime = number2;

        rowAccum++;
    }
    return largestPrime;
};