type ToBeOrNotToBe = {
    toBe: (val: any) => boolean;
    notToBe: (val: any) => boolean;
};

const expect = (val: any): ToBeOrNotToBe => ({
    toBe: (val2: any) => {
        if (val === val2) return true;
        throw new Error('Not Equal');
    },
    notToBe: (val2: any) => {
        if (val !== val2) return true;
        throw new Error('Equal');
    }
});

/**
 * expect(5).toBe(5); // true
 * expect(5).notToBe(5); // throws "Equal"
 */