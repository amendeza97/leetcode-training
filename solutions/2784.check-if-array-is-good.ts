function isGood(nums: number[]): boolean {
    const map = {}, n = nums.length - 1;
    let max = 0;
    for (let idx = 0; idx < nums.length; idx++) {
        let num = nums[idx];
        map[num] = (map[num] ?? 0) + 1;
        if (num > max) max = num;
        if (map[num] > 1 && (num !== n || max > n || map[num] > 2)) {
          return false
        }
    }
    return map[max] === 2;
};