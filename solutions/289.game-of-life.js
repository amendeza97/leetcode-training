const calculateNexGenCell = (idxRow, idxCol, board) => {
    let sum = 0;
    sum += (board[idxRow - 1]?.[idxCol] ?? 0);
    sum += (board[idxRow - 1]?.[idxCol - 1] ?? 0);
    sum += (board[idxRow - 1]?.[idxCol + 1] ?? 0);
    sum += (board[idxRow]?.[idxCol - 1] ?? 0);
    sum += (board[idxRow]?.[idxCol + 1] ?? 0);
    sum += (board[idxRow + 1]?.[idxCol] ?? 0);
    sum += (board[idxRow + 1]?.[idxCol - 1] ?? 0);
    sum += (board[idxRow + 1]?.[idxCol + 1] ?? 0);

    const currentCell = board[idxRow][idxCol];
    if (currentCell === 1) {
        return (sum < 2 || sum > 3) ? 0 : 1;
    }

    return sum === 3 ? 1 : 0;
}

/**
 * @param {number[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 */
var gameOfLife = function (board) {
    const auxBoard = JSON.parse(JSON.stringify(board));
    const rowsLength = board.length, colsLength = board[0].length;
    let rowIndex = 0, colIndex = 0;
    let exit = false;
    while (!exit) {
        const newValue = calculateNexGenCell(rowIndex, colIndex, auxBoard);
        board[rowIndex][colIndex] = newValue;
        exit = (rowIndex + 1) === rowsLength && (colIndex + 1) === colsLength;

        if (++colIndex === colsLength) {
            colIndex = 0;
            rowIndex++;
        }
    }
};