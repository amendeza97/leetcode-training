/**
 * @param {string} s
 * @return {number}
 */
var scoreOfString = function(s) {
    let result = 0;
    const [firstChar, ...restOfChars] = s.split('');
    let previousScore = firstChar.charCodeAt(0);
    for (let char of restOfChars) {
        let numericValue = char.charCodeAt(0);
        result += Math.abs(previousScore - numericValue);
        previousScore = numericValue;
    }
    return result;
};