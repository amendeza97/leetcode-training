func countCompleteDayPairs(hours []int) int {
	var counter = 0
	for idxLeft := 0; idxLeft < len(hours); idxLeft++ {
		for idxRight := idxLeft + 1; idxRight < len(hours); idxRight++ {
			if (hours[idxLeft]+hours[idxRight])%24 == 0 {
				counter++
			}
		}
	}
	return counter
}