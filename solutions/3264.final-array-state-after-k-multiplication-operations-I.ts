function getFinalState(nums: number[], k: number, multiplier: number): number[] {
    if (!k) return nums;

    let index = 0;
    let min = nums[index];
    for (let i = 1; i < nums.length; i++) {
        const num = nums[i];
        if (num < min) {
            min = num;
            index = i;
        }
    }
    nums[index] = nums[index] * multiplier;
    return getFinalState(nums, k - 1, multiplier);
};