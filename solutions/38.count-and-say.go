import "fmt"

func recursiveCountAndSay(n int, result *string) {
	if n == 1 {
		return
	}

	if *result == "1" {
		*result = fmt.Sprintf("%s%s", "1", *result)
		recursiveCountAndSay(n-1, result)
		return
	}

	var newResult = ""
	var auxResult = *result
	var lastValue, counter = string(auxResult[0]), 1
	for index := 1; index < len(auxResult); index++ {
		var currentValue = string(auxResult[index])
		if currentValue != lastValue {
			newResult = fmt.Sprintf("%s%d%s", newResult, counter, lastValue)
			lastValue = currentValue
			counter = 1
		} else {
			counter++
		}
	}
	newResult = fmt.Sprintf("%s%d%s", newResult, counter, lastValue)
	*result = newResult
	recursiveCountAndSay(n-1, result)
}

func countAndSay(n int) string {
	var result = "1"
	recursiveCountAndSay(n, &result)
	return result
}
