func isSubsequence(s string, t string) bool {
	var sChars = []rune(s)
	var tChars = []rune(t)
	var idxS = 0

	for idxT := 0; idxT < len(t) && idxS < len(s); idxT++ {
		var tChar = string(tChars[idxT])
		var sChar = string(sChars[idxS])
		if tChar == sChar {
			idxS++
		}
	}

	return idxS == len(s)
}