/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

 func sumRecursive(node *TreeNode, result *int) {
    if node.Left != nil {
        if node.Left.Left == nil && node.Left.Right == nil {
            *result = *result + node.Left.Val;
        } else {
            sumRecursive(node.Left, result)
        }
    }
    if node.Right != nil {
        sumRecursive(node.Right, result)
    }
}

func sumOfLeftLeaves(root *TreeNode) int {
    var sum = 0;
    sumRecursive(root, &sum);
    return sum;
}