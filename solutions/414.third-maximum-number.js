/**
 * @param {number[]} nums
 * @return {number}
 */
var thirdMax = function(nums) {
    nums.sort((a, b) => b - a);
    const [firstMax, ...restNums] = nums;

    let [nextMax, counter] = [firstMax, 1];
    for (num of restNums) {
        if (num < nextMax) {
            counter++;
        }
        nextMax = num;
        if (counter === 3) return num;
    }
    return firstMax;
};