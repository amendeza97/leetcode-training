func islandPerimeter(grid [][]int) int {
	var colLength = len(grid[0])
	var rowLength = len(grid)
	var idxCol = 0
	var idxRow = 0
	var shouldContinue = true
	var perimeter = 0
	for shouldContinue {
		var point = grid[idxRow][idxCol]
		if point == 1 {
			if idxRow == 0 || grid[idxRow-1][idxCol] == 0 {
				perimeter++
			}
			if idxCol == 0 || grid[idxRow][idxCol-1] == 0 {
				perimeter++
			}
			if idxRow == (rowLength-1) || grid[idxRow+1][idxCol] == 0 {
				perimeter++
			}
			if idxCol == (colLength-1) || grid[idxRow][idxCol+1] == 0 {
				perimeter++
			}
		}
		idxCol++
		if idxCol == colLength {
			idxCol = 0
			shouldContinue = idxRow < (rowLength - 1)
			idxRow++
		}
	}
	return perimeter
}