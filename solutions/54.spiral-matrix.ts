function spiralOrder(matrix: number[][]): number[] {
  let result: number[] = [];
  const expectedLength = matrix.length * matrix[0].length;
  let r = 0, c = 0;
  let mode = 'r';

  while (result.length < expectedLength) {
      switch(mode) {
          case 'r':
            const [row] = matrix.splice(0,1);
            result = [...result, ...row];
            mode = 'd';
            r = 0;
            c = matrix[0]?.length - 1;
            continue;
          case 'd':
            result.push(matrix[r][c]);
            matrix[r].splice(c, 1);
            if (matrix[r + 1]?.[c] !== undefined) {
              r++;
            } else {
              mode = 'l';
            }
            continue;
          case 'l':
            const [lastRow] = matrix.splice(matrix.length - 1, 1);
            result = [...result, ...lastRow.reverse()];
            mode = 'u';
            r = matrix?.length - 1;
            c = 0;
            continue;
          case 'u':
            result.push(matrix[r][c]);
            matrix[r].splice(c, 1);
            if (matrix[r - 1]?.[c] !== undefined) {
              r--;
            } else {
              mode = 'r';
            }
            continue;
          default:
            break;
      }
  }
  return result;
};