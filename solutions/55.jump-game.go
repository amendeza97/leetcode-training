func canJump(nums []int) bool {
	var idxGoal = len(nums) - 1
	var canJump = true
	for idx := idxGoal - 1; idx >= 0; idx-- {
		canJump = (idx+nums[idx] >= idxGoal)
		if canJump {
			idxGoal = idx
		}
	}
	return canJump
}