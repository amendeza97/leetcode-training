func canPlaceFlowers(flowerbed []int, n int) bool {
	var increment uint8 = 1
	var index uint16
	var length uint16 = uint16(len(flowerbed))
	var flowersCounter int = n
	for index = 0; index < length && flowersCounter > 0; index += uint16(increment) {
		if flowerbed[index] == 0 {
			if index+1 < length {
				if flowerbed[index+1] == 0 {
					flowersCounter--
					increment = 2
					continue
				}
			} else {
				flowersCounter--
				increment = 2
				continue
			}
			increment = 1
		} else {
			increment = 2
		}
	}
	return flowersCounter == 0
}