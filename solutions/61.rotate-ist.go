/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func rotateRight(head *ListNode, k int) *ListNode {
	if head == nil {
		return head
	}
	var counter = 1
	var prev *ListNode
	var newHead = head

	for newHead.Next != nil {
		prev = newHead
		newHead = prev.Next
		counter++
	}
	newHead.Next = head

	var limit = counter - k

	for limit <= 0 {
		limit = counter + limit
	}

	var aux = head
	counter = 1

	for counter < limit {
		aux = aux.Next
		counter++
	}

	var result = aux.Next
	aux.Next = nil
	return result
}