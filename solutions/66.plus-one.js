/**
 * @param {number[]} digits
 * @return {number[]}
 */
var plusOne = function(digits) {
    let accum = 1;
    let newData = digits.reverse();
    for (let index = 0; index < digits.length; index++) {
        const digit = newData[index];
        const sum = digit + accum;
        const shouldAccum = sum > 9;
        if (shouldAccum) {
            accum = 1;
            newData[index] = 0;
        } else {
            accum = 0;
            newData[index] = sum;
            break;
        }
    }

    if (accum) {
        newData.push(accum);
    }
    return newData.reverse();
};
