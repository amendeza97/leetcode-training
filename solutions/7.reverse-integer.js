/**
 * @param {number} x
 * @return {number}
 */
var reverse = function(x) {
    let rest = x;
    let result = 0;
    while (rest !== 0) {
        const lastDigit = rest % 10;
        result = (result * 10) + lastDigit;
        rest = Math.trunc(rest / 10)
    }

    if (result < -2147483648 || result > 2147483647) {
        return 0;
    }
    return result;
};