/**
 * @param {number[]} nums
 * @return {boolean}
 */
var isMonotonic = function(nums) {
    const numsLength = nums.length;
    const isIncreasing = nums[0] > nums[numsLength - 1];
    for (let index = 0; index < numsLength - 1; index++) {
        const [left, right] = [nums[index], nums[index + 1]];
        const pairResult = isIncreasing ? left >= right : right >= left;
        if (!pairResult) return false;
    }
    return true;
};