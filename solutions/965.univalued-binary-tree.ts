/**
 * Definition for a binary tree node.
 * class TreeNode {
 *     val: number
 *     left: TreeNode | null
 *     right: TreeNode | null
 *     constructor(val?: number, left?: TreeNode | null, right?: TreeNode | null) {
 *         this.val = (val===undefined ? 0 : val)
 *         this.left = (left===undefined ? null : left)
 *         this.right = (right===undefined ? null : right)
 *     }
 * }
 */

function isUnivalTree(root: TreeNode | null, num?: number): boolean {
    if (!root) return false;
    if (!isNaN(num) && root.val !== num) return false;

    const treeNumber = num ?? root.val;
    const isLeftUnivalSubtree = root.left ? isUnivalTree(root.left, treeNumber) : true;
    if (!isLeftUnivalSubtree) return false;

    const isRightUnivalSubtree = root.right ? isUnivalTree(root.right, treeNumber) : true;
    return isLeftUnivalSubtree && isRightUnivalSubtree;
};